package edu.hope.cs.csci392.imdb.model;

import edu.hope.cs.csci392.imdb.Database;

public class Role {
	private int actorID;
	private int movieID;
	private String role;
	private Movie movie;
	private Actor actor;
	
	public Role (int actorID, int movieID, String role) {
		this.actorID = actorID;
		this.movieID = movieID;
		this.role = role;
		movie = null;
		actor = null;
	}

	public int getActorID() {
		return actorID;
	}

	public int getMovieID() {
		return movieID;
	}

	public String getRole() {
		return role;
	}
	
	public Actor getActor () {
		if (actor == null) {
			Database database = Database.getInstance();
			actor = database.findActorByID(actorID);
		}
		return actor; 
	}
	
	public Movie getMovie () {
		if (movie == null) {
			Database database = Database.getInstance();
			movie = database.findMovieByID(movieID);
		}
		return movie; 
	}
}